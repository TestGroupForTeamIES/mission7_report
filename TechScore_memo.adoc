:toc: left
:sectnums:
:toc-title: 目次
:toclevels: 2
:icons: font
:author: チームIES　羅
:revnumber: 1.0

:path_site: http://www.techscore.com/tech/sql/index.html/
:path_mission5: https://gitlab.com/TestGroupForTeamIES/mission5_report/blob/master/Mission5_Report.pdf

<<<

= CH9、ビュー

== ビューとは
====
* CREATE TABLE で定義された実テーブルから作成される仮想的なテーブルのこと
====

== ビューの作成

	CREATE VIEW ビュー名 [(列名, 列名, .....)] AS
	       SELECT文
	       [WITH CHECK OPTION];

====
* [WITH CHECK OPTION] はビューの更新に関連するオプション
====

例：

	  CREATE VIEW v_受注 AS
	    SELECT JJ.受注番号, KK.顧客名, SS.商品名,
	           JJ.受注個数, JJ.納品日
	      FROM 受注表 JJ, 顧客表 KK, 商品表 SS
	      WHERE JJ.顧客コード = KK.顧客コード
	            AND JJ.商品コード = SS.商品コード ;


確認：

	SELECT * FROM v_受注 ;

== ビューの変更

====
* ビューはデータの入っていない仮想的なテーブルなので、実際にはビューの元になる実テーブルのデータが変更されること
* ビューは実テーブルから仮想的な表として定義されているものなので、実テーブルに対してデータの追加、更新、削除を行うように自由にはできない
====


== ビューの削除
====
* ビューは削除されるが、ビューの元になる実テーブルのデータは削除されない
====

	DROP VIEW v_受注 ;

<<<

= データベースの管理

====
* RDBMS はデータベースを利用できるユーザを限定することにより、データベースを保護する
====

|===
image:http://www.techscore.com/page_attachments/0000/0237/sql101.gif[]
|===

== ユーザの作成

	CREATE USER ユーザ名 IDENTIFIED BY パスワード ;
	
====
* ユーザを作成すると同時に、スキーマが自動的に作成される
* スキーマ： +
テーブルなどのオブジェクトを格納する器 +
特定のユーザが所有するオブジェクトの集合 +
====

スキーマの作成

	CREATE SCHEMA AUTHORIZATION スキーマ名 ;

|===
image:http://www.techscore.com/page_attachments/0000/0239/sql102.gif[]
|===

スキーマの参照

	SELECT * FROM BBB.yyy ;
	

== アクセス権限

====
* スキーマに作成されたテーブルやビューなどのオブジェクトを利用するための権利
====

SQL92 で定義されているアクセス権限

====
* SELECT： +
データの参照

* INSERT： +
データの追加

* UPDATE： +
データの変更・修正

* DELETE： +
データの削除

* REFERENCES： +
他テーブルの外部キーを作成

* ALL： +
すべての操作
====

権限を与える

	GRANT 権限名 ON オブジェクト名
	      TO { スキーマ名 | PUBLIC }
	      [ WITH GRANT OPTION ] ;

例

	 GRANT INSERT,UPDATE ON BBB.yyy
	       TO CCC ;
	       
|===
image:http://www.techscore.com/page_attachments/0000/0245/sql1034.gif[]
|===

アクセス権限を取り消す

	REVOKE 権限名 ON オブジェクト名
	      FROM { スキーマ名 | PUBLIC } ;

例

	REVOKE ALL ON BBB.yyy
	       TO CCC ;

|===
image:http://www.techscore.com/page_attachments/0000/0247/sql1033.gif[]
|===

<<<

= トランザクション

====
* INSERT 文 +
データの追加・更新・削除のSQL文

* トランザクション +
「UPDATE 文」「DELETE 文」
====

== トランザクションの特性 +

====
* 原子性 (ATOMICITY) +
1．トランザクションは分割することのできない最小の作業単位である +
2．トランザクションを構成する処理の結果がすべて有効になるか、またはすべて無効になる

* 一貫性 (CONSISTENCY) +
トランザクションで処理されるデータは、実行前と後でデータの整合性を持つ

* 隔離性 (ISOLATION) +
処理対象が同じデータである複数のトランザクションを一度に実行する場合は、それぞれのトランザクションは隔離された (独立された) 状態でデータの変更を行う

* 持続性 (DURABILITY) +
トランザクションで処理されるデータの状態はトランザクションが終了するまで変化しない
====

== コミットとロールバック

====
* コミット： +
トランザクションによるデータの変更処理が正常に終了した場合に、その変更処理を有効な結果と確定し、データベースに反映すること

* ロールバック： +
トランザクションによるデータの変更処理の途中で何らかの障害が発生した場合に、それまでの変更処理を無効なものとし、トランザクションが実行される前の状態にもどすこと
====

|===
IMAGE:http://www.techscore.com/page_attachments/0000/0257/sql1122.gif[]
|===

== トランザクションの構文

====
* 「INSERT 文」「UPDATE 文」「DELETE 文」のいずれかのデータ操作文を実行した時点で、自動的にトランザクションが開始される
====

例

	(BEGIN WORK ; )       /* PostgreSQL の場合       */	 
	INSERT 文 ;           /* トランザクションの開始 */	 
	COMMIT ;              /* 処理の確定             */
	 
	(BEGIN WORK ; )       /* PostgreSQL の場合       */	 
	UPDATE 文 ;           /* トランザクションの開始 */	 
	ROLLBACK ;            /* 処理の取り消し         */
	 
	(BEGIN WORK ; )       /* PosgreSQL の場合        */	 
	DELETE 文 ;           /* トランザクションの開始 */	 
	SAVEPOINT sPOINT ;    /* セーブポイントの設定   */	 
	INSERT 文 ;	 
	ROLLBACK TO SAVEPOINT sPOINT ;
	                      /* セーブポイントへ戻る   */	 
	INSERT 文 ;	 
	COMMIT ;              /* 処理の確定             */




== 排他制御

====
* あるトランザクションが実行中のときに、そのトランザクションが対象としているデータをロックし、他のトランザクションのアクセスを禁止することによって、データの整合性を確保しようとする仕組み
====

<<<

|===
image:http://www.techscore.com/page_attachments/0000/0261/sql1142.gif[]
|===


== ロック

====
* 共有ロック +
1．トランザクションが SELECT 文によりデータを参照するものであるときのロックのモード +
2．共有ロックによりデータがロックされている間は、他のトランザクションからデータを参照することはできるが、データの変更はできない

* 排他ロック +
トランザクションが INSERT 文、UPDATE 文及び DELETE 文であるときのロックのモード +
排他ロックされている間、他のトランザクションからデータの参照も変更もできない
====

デッドロック

|===
image:http://www.techscore.com/page_attachments/0000/0263/sql1151.gif[]
|===



== トランザクションの定義


SET TRANSACTION 文を用いて、次の項目について設定を行う +
====
* アクセスモード +
トランザクションが読み取り専用であるのか、読み取り / 書き込み可能なのかを指定する
初期値は READ WRITE(読み取り / 書き込み可能)

* 隔離レベル +
ACID 特性の隔離性 (ISOLATION) のレベルを指定する
初期値は READ COMMITTED

* 診断域 +
SQL 文の処理結果に関する情報を格納する領域を指定する
====

	
SET TRANSACTION の基本構文

	<code>SET TRANSACTION
	  [ {READ ONLY | READ WRITE} ]
	  [ ISOLATION LEVEL
	    {READ UNCOMMITTED | READ COMMITTED |
	     REPEATABLE READ | SERIALIZABLE}]
	  [ DIAGNOSTICS SIZE 行数 ] ;</code>


アクセスモード

====
* READ ONLY +
読み取り専用

* READ WRITE +
読み書き両用(初期値)
====

隔離レベル

====
* READ UNCOMMITTED +
共有ロック、排他ロックも使用されない、最も隔離性の緩やかなレベル +
ダーティリード、ノンリピータブルリード、ファントムインサートが発生する可能性がある

* READ COMMITTED +
トランザクションが開始されて、まだコミットされていない場合には、他のトランザクションからはデータを参照することができないレベル +
ノンリピータブルリード、ファントムインサートが発生する可能性がある

* REPEATABLE READ +
トランザクションが完了するまで共有ロックが継続されるので、一度読み取ったデータが他のトランザクションによって変更されることはない +
ファントムインサートが発生する可能性がある

* SERIALIZABLE +
他のトランザクションからは完全に隔離したレベルで、他のトランザクションの影響を一切うけない +
トランザクション実行中に、他のトランザクションがアクセスしても、そのトランザクションは待機状態になり、前のトランザクションが終了するまでデータにアクセスすることができない
====

隔離レベルにより発生し得る問題

====
* ロストアップデート +
あるトランザクションで変更した値が、他のトランザクションにより別の値に変更されてしまうこと

* ダーティリード +
他のトランザクションがデータの変更を取り消したにも関わらず、取り消し前にそのデータを読み出してしまうこと

* ノンリピータブルリード +
あるトランザクションにおいて、同じ行を 2 回読み込んだときに、1 回目と 2 回目の読み込みの間に、他のトランザクションが行の値の変更を行ったために、1 回目と 2 回目で読み込んだデータの値が異なってしまうこと

* ファントムインサート +
あるトランザクションにおいて、WHERE 句等による同じ条件により行を読み込んだときに、その途中に他のトランザクションがその条件に影響を与える行の挿入を行ったために、先に読み込んだ行と後に読み込んだ行が異なってしまうこと
====



ロストアップデートの例

|===
image:http://www.techscore.com/page_attachments/0000/0265/sql1160.gif[] +
TransactionA と TransactionB が、ColA=10 であることを確認した後に、TransactionA が ColA の値を 10 から 8 に変更する。しかし、その直後に TransactionB が ColA の値を 15 に変更してしまうと、TransactionA の行ったデータの更新はデータに反映されなかったこと
|===



ダーティリードの例

|===
image:http://www.techscore.com/page_attachments/0000/0267/sql1161.gif[] +
TransactionA が TableA の ColA の値を最初に確認した後に、UPDATE 文で ColA の値を 10 から 15 に変更します。この後で、TransactionB は ColA の値を読み取る。その後、TransactionA が変更処理を ROLLBACK 文で変更してしまったとき、TransactionB の読み取った値は、確定されなかったものになってしまう
|===

ノンリピータブルリードの例

|===
image:http://www.techscore.com/page_attachments/0000/0269/sql1162.gif[] +
TransactionB が ColA の値を読み取った後で、TransactionA が ColA の値を 10 から 15 に変更し、その後、TransactionB が再び ColA の値を読み取ると、TransactionB は同じ行に対して 1 回目と 2 回目で異なる値を読み取ってしまうこと
|===

ファントムインサート

|===
image:http://www.techscore.com/page_attachments/0000/0271/sql1163.gif[] +
TransactionB が TableA の ColA 列の値の平均を読み取った後で、TransactionA が行を追加し、その後、TransactionB が再び TableA の ColA 列の値の平均を読み取ろうとすると、TransactionB は以前と異なる値を読み取ってしまうこと
|===

まとめ
|===
image:https://gitlab.com/TestGroupForTeamIES/mission7_report/raw/master/PIC/15.png[]
|===



= カーソル

== 外部アプリケーションからの SQL 呼び出し

====
* 一つの命令が完結した意味を持ち、それだけでデータベースを処理すること
* コマンド入力により、対話方式でデータベースに処理が行える
====

他の言語と組み合わせて SQL を呼び出す手順

	1．モジュール呼び出し
	2．静的 SQL
	3．動的 SQL

モジュール呼び出し

|===
image:http://www.techscore.com/page_attachments/0000/0273/sql1211.gif[] +
SQL によって作成されたモジュールを、ホスト言語からサブルーチンとして呼び出す方法
|===

静的 SQL
|===
image:http://www.techscore.com/page_attachments/0000/0275/sql1212.gif[] +
* ホスト言語によって書かれるプログラムソース内に、直接 SQL 文を記述するもの +
* パフォーマンスは静的 SQL のほうが優れている +
|===

動的 SQL
|===
image:http://www.techscore.com/page_attachments/0000/0277/sql1213.gif[] +
* SQL が変化するもの +
* 柔軟性の高いアプリケーションを開発することができる +
|===

<<<


== カーソル

====
* クエリの結果集合を一時的に蓄えておくための仮想的な作業領域のこと
* 使用手順： +
1．カーソルの宣言 +
2．カーソルを開く +
3．一行ごとにデータを取り出す(ループ処理) +
4．カーソルを閉じる
====

|===
image:http://www.techscore.com/page_attachments/0000/0279/sql1221.gif[]
|===

DECLARE CURSOR 文で宣言

	DECLARE カーソル名 [ INSENSITIVE ] [ SCROLL ] CURSOR
	   FOR { SELECT文 [ 更新可能性句 ] } | {準備された文} ;

====
* INSENSITIVE +
指定するとカーソルを開いた後、カーソルの内容が固定される

* SCROLL +
後に述べる FETCH 文の「方向」で NEXT 以外も指定することができる

* 更新可能性句 +
そのカーソルが更新可能なのか、読み取り専用なのかを指定するもの

* 準備された文 +
あらかじめ PREPARE 文により SQL 文と文字列を関連付けておき、その文字列を記述
====

更新可能性句の例

	FOR { READ ONLY | UPDATE [ OF 列リスト] }
	
====
* UPDATE +
指定すると、更新可能なカーソルと定義される
====

PREPARE 文の基本構文

	PREPARE [ GLOBAL | LOCAL ] SQL文名 FROM 文字列変数 ;

カーソルを開く方法、OPEN CURSORの基本構文

	OPEN カーソル名 [ USING 値ソース ] ;

====
* USING 句 +
動的 SQL の場合にのみ指定
====

値ソースの指定

	パラメータ | { SQL DESCRIPTOR 記述子 }
	

FETCHの基本構文

	FETCH [ [ 方向 ] FROM ] カーソル名 INTO 変数リスト ;

====
* 方向 +
行を示すポインタがどこに移動した後に、行のデータを読み取るのかを指定するもの
====

方向指定の仕方

	NEXT | PRIOR | FIRST | LAST |
	{ ABSOLUTE | RELATIVE オフセット }

カーソルを閉じる構文

	CLOSE カーソル名 ;


== ストアドプロシージャ

====
* データベースに対する一連の処理を一つのプログラムにまとめ (PROCEDURE)、データベース管理システム (RDBMS) に保存 (STORE) したもの
====

=== ストアドプロシージャの基本構文

ストアドプロシージャの作成

	CREATE [ OR REPLACE ] PROCEDURE ストアドプロシージャ名
	       [ (引数名 データ型),... ]
	IS     [ 変数名 データ型,... ]
	BEGIN
	       処理内容
	END ;
	

ストアドプロシージャの実行

	BEGIN
	     [ EXECUTE ] ストアドプロシージャ名 [(引数,...)]
	END ;
	
ストアドプロシージャの削除

	DROP PROCEDURE プロシージャ名 ;
	
ストアドプロシージャの利用

	/* ストアドプロシージャの定義 */
	 
	 CREATE PROCEDURE pro_200
	 IS
	 BEGIN
	     SELECT DISTINCT KK.顧客名 FROM 受注表 JJ,顧客表 KK
	         WHERE JJ.顧客コード = KK.顧客コード
	               AND JJ.受注個数 >= 200 ;
	 END ;
	 
	/* ストアドプロシージャの実行 */
	 
	 BEGIN
	       pro_200
	 END ;

IF 文でストアドプロシージャを制御する例


	/* ストアドプロシージャの定義 */
	 
	 CREATE PROCEDURE pro_BIG(KOSUU NUMBER)
	 IS
	     SHOHIN_COUNT NUMBER ;
	 BEGIN
	     SELECT COUNT(*) INTO SHOHIN_COUNT FROM 受注表
	         WHERE 受注個数 > KOSUU ;
	     IF SHOHIN_COUNT = 0 THEN
	         SELECT 'NO DATA' ;
	     ELSE
	         SELECT 受注番号,受注個数
	             FROM 受注表
	             WHERE 受注個数 > KOSUU
	             ORDER BY 受注個数 ;
	     END IF ;
	 END ;
	 
	/* ストアドプロシージャの実行 */
	 
	 BEGIN
	       pro_BIG(150)
	   
<<<

= トリガー

====
* 表に対して何らかの変更処理が加えられたときに、その変更処理をきっかけとして自動的に実行される特殊なストアドプロシージャのこと
====

== トリガーの基本構文

	CREATE [ OR REPLACE ] TRIGGER トリガー名
	       { BEFORE | AFTER | INSTEAD OF }
	       { INSERT | UPDATE [OF 列名,...] | DELETE }
	         [OR {INSERT | UPDATE [OF 列名,...] | DELETE }]
	         [... ]
	       ON テーブル名
	       [ FOR EACH ROW ]
	       [ WHEN 条件式 ]
	       BEGIN
	           処理内容
	       END ;

トリガーの有効 / 無効を切り替えるSQL

	ALTER TRIGGER トリガー名 { ENABLE | DISABLE | COMPILE } ;

トリガーを削除

	DROP TRIGGER トリガー名 ;


= インデックス

====
* データの検索速度を向上させるために、どの行がどこにあるかを示した索引のこと
====

== インデックスの基本構文

インデックスを作成

	CREATE [UNIQUE] INDEX インデックス名
	       ON テーブル名(列名 [ ASC | DESC ],...) ;

インデックスを削除

	DROP INDEX インデックス名 ;

インデックスの注意点

====
* パフォーマンス向上 +
表の結合条件に使用される列に対してインデックスを作成するとパフォーマンスが向上する +
値の分布が大きいとは、異なる値が多いということなので、値の分布が大きな列に対してインデックスを作成するとパフォーマンスが向上する +


* パフォーマンス低下 +
値の分布が小さな列に対してインデックスを作成するとパフォーマンスが低下する +
テーブルを更新すると、インデックスも更新されるので、テーブルが頻繁に更新されるような場合にインデックスを利用するとパフォーマンスは低下する

* その他 +
インデックスは表のデータとは別の領域に保存されるので、データベース設計時にはインデックスの領域も見込む必要がある
インデックスは表のデータに対して作成するもので、ビューのデータに対しては作成することができないが、ビューに対してデータの検索を行う場合は、元のテーブルに作成されたインデックスが利用される
====
<<<


= データベースの設計

== ER(Entity-Relationship) モデル

====
* エンティティとエンティティとリレーションシップの関係を図示し、分析する技法
* 対応関係のパタン +
1:1 +
1:N +
m:n
====

|===
image:http://www.techscore.com/page_attachments/0000/0283/sql1632.gif[]
|===


== 正規化

以前のlink:{path_mission5}[メモ]を参考

== ボイスコッド正規形

====
* すべての列が主キーに完全関数従属で、他に完全関数従属関係がないもの
====

